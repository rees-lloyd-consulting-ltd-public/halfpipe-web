import ValidateFields from "./ValidateFields";

export default class FieldValidator
{
    private static getValue = (field: Element) => {
        let val = "";
        if((field as HTMLInputElement).value)
        {
            val = (field as HTMLInputElement).value;
        }
        return val;
    };

    public static searchForParent(field: Element, className: string): Element {
        let currentAncestor: Element = field;
        do {
            if (currentAncestor.parentElement !== null) {
                currentAncestor = currentAncestor.parentElement;
            } else {
                break;
            }
        }
        while (currentAncestor && currentAncestor.parentElement && !currentAncestor.classList.contains(className));
        return currentAncestor;
    };

    public static notBlank = (field: Element) => {
        let retval = /\S/.test(FieldValidator.getValue(field));
        return retval;
    };

    public static email = (field: Element) => {
        return !FieldValidator.notBlank(field) ||
            /^.+@.+\..+$/.test(FieldValidator.getValue(field));
    };

    public static phone = (field: Element) => {
        return !FieldValidator.notBlank(field) ||
            /^\(?\d{3}\)?[- ]?\d{3}[- ]?\d{4}$/.test(FieldValidator.getValue(field));
    };

    public static number = (field: Element) => {
        let v = FieldValidator.getValue(field);
        return !FieldValidator.notBlank(field) ||
            /^[+-][0-9.,]+$/.test(v)
            && !isNaN(parseFloat(v.replace(/[^0-9.,]/g, "")));
    };

    public static oneIsChecked = (field: Element) => {
        let retval = false;
        // Find all children of the same type as this element and within the same parent field.
        let p = FieldValidator.searchForParent(field, 'field');
        // Find all input tags under the parent.
        let inputs = p.getElementsByTagName('input');
        for(let idx = 0; idx < inputs.length; idx++) {  // for each input element...
            if(inputs[idx].checked) {  // if the input it is checked...
                retval = true;
            }
        }
        return retval;
    };

    public static oneIsSelected = (field: Element) => {
        let retval = false;
        // Find all children of the same type as this element and within the same parent field.
        let p = FieldValidator.searchForParent(field, 'field');
        // Find all input tags under the parent.
        let inputs = p.getElementsByTagName('option');
        for(let idx = 0; idx < inputs.length; idx++) {  // for each option element...
            if(inputs[idx].selected && !inputs[idx].disabled) {  // if an option is selected...
                retval = true;
            }
        }
        return retval;
    };
}