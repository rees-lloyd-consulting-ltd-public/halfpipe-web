---
title: "Halfpipe Survey"
layout: funnel-survey
image: images/richard-portrait-1.jpeg
altText: Image of Richard Lloyd
redirectTo: /thanks
heading: |
    Step 2 of 2: Please Complete This Quick Survey To Reserve Your 
    Free Strategy Session
subheading: | 
    Please fill out the form below so we can prepare for
    your appointment. If we don't receive an application from you, 
    your appointment will unfortunately be cancelled
---
