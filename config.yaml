baseURL: https://halfpipe.sh
#baseURL: https://rees-lloyd-consulting-ltd-public.gitlab.io/halfpipe-web/
languageCode: en-gb
title: "Halfpipe | Data Platforms"
theme: hugo-fresh
googleAnalytics: # see also googleTagManager below; supply UA-XXXXXXX number to enable the internal GA tag template; see also meta.html
enableRobotsTXT: true
# Disables warnings
disableKinds:
  - taxonomy
  - taxonomyTerm
markup:
  goldmark:
    renderer:
      unsafe: true # Allows you to write raw html in your md files
enableGitInfo: true
params:
  globalDateFormat: "02 Jan, 2006"
  googleTagManager: GTM-T5V79K4
  # Open graph allows easy social sharing. If you don't want it you can set it to false or just delete the variable
  openGraph: true
  # Used as meta data; describe your site to make Google Bots happy 
  description: Your data in the cloud using smart, lightweight tech that's easy to run.
  navbarlogo:
    # H icon:
    image: /images/halfpipe-logo-300px.png
    link: /
  font:
    name: "Exo 2"
    sizes: [400,700]
  linkedInUrl: https://www.linkedin.com/in/rich4rdlloyd
  gitHubUrl: https://github.com/relloyd/halfpipe
  hero:
    # Main hero title
    title: |
      Your Data In The Cloud Using Smart, Lightweight Tech
    # Hero subtitle (optional)
    subtitle: |
      Migrate to Snowflake rapidly using tech that's easy to understand. It's compatible with on-prem, cloud-compute and serverless architectures.
      <br><br>
      With ready-made data integration and validation patterns available for all engineers to use, deploy and scale, it's the micro-service to super-power your business.
    # Button text
    buttontext: Learn More >>>
    # Where the main hero button links to
    buttonlink: /in-detail
    # Hero image (from static/images/___)
    image: hp-commands-3.gif
    imageAltText: Rip An Oracle Schema In One Command
    mainLogo: /images/halfpipe-logo-2000px.png
    clientlogos:
      - /images/postgresql.png
      - /images/microsoft-sql-server.png
      - /images/oracle-logo-text-large-and-round-logo.png
      - /images/snowflake-logo-1024px.png
  # Customizable navbar. For a dropdown, add a "sublinks" list.
  navbar:
    #  - title: HOW IT WORKS
    #    url: /#how-it-works
    #    sublinks:
    #    - title: The Halfpipe Viewpoint
    #      url: /view-point
    #    - title: Features
    #      url: /features
    #    - title: Architecture
    #      url: /how-it-works
    #  - title: DOWNLOAD
    #    url: https://github.com/relloyd/halfpipe/releases
    #    newWindow: true
    - title: HOW TO BUY
      url: /services
    - title: IN DETAIL
      url: /in-detail
    - title: BLOG
      url: /blog
    - title: ABOUT THE CREATOR
      url: /about
    - title: CONTACT
      url: /contact
    - title: Get Started >>>
      url: /about
      button: true
  contactForm: true
  disclaimers:
    socialMediaDisclaimer: |
      This site is not a part of the LinkedIn website or LinkedIn Corporation. Additionally, this site is NOT endorsed by LinkedIn in any way.
      LinkedIn is a trademark of LinkedIn Corporation and its subsidiaries.
    privacyDisclaimer: |
      By submitting this form, I acknowledge that I understand my contact information will be shared with halfpipe.sh
      and subject to its [Privacy Policy](/privacy), as well as Google and be subject to Google’s [Privacy Policy](https://policies.google.com/privacy).
    infoSafetyDisclaimer: |
      Your information is safe and is not sold to third parties.
  footer:
    # Logo (from /images/logos-try-1/___)
    # H logo:
    # logo: /images/logos-try-1/android-chrome-192x192.png
    logo: /images/halfpipe-hourglass-192px.png
    # Social Media Title
    socialmediatitle: Follow Us
    # Social media links (GitHub, Twitter, etc.). All are optional.
    socialmedia:
      # Icons are from Font Awesome
      - link: https://www.linkedin.com/in/rich4rdlloyd/
        icon: linkedin
      - link: https://github.com/relloyd/halfpipe
        icon: github
      - link: https://twitter.com/halfpipedata
        icon: twitter
    bulmalogo: false
    copyright: "© 2020 Halfpipe. All Rights Reserved, Rees Lloyd Consulting Ltd"
    quicklinks:
      column1:
        title: "Product"
        links:
          - text: In Detail
            link: /in-detail
          - text: Learn Halfpipe
            link: /in-detail/#learn
          - text: Discover Features
            link: /features
          - text: Why choose Halfpipe?
            link: /in-detail
      column2:
        title: "Services"
        links:
          - text: Enterprise Solutions
            link: /services
      column3:
        title: "Contact"
        links:
          - text: Schedule a Call
            link: /contact/#schedule
          - text: By Email
            link: /contact/#contact
      column4:
        title: "Legal"
        links:
          - text: Terms of Service
            link: /terms
          - text: Privacy Policy
            link: /privacy
          - text: Cookies
            link: /cookies
  whatsItGoodFor:
    title: What's It Good For?
    subtitle: |
      Halfpipe helps businesses migrate rapidly from Oracle To Snowflake, using simple tech that's compatible
      with on-prem, cloud-compute and serverless architectures
    tiles:
      - title: Migrate A Schema In One Command
        icon: mouse-globe
        text: Simple to use both locally and in production
        url: /sign-up
        buttonText: Get started
      - title: Keep Your Data Up-To-Date Easily
        icon: laptop-cloud
        text: Take snapshots or deltas periodically, in one command
        url: /sign-up
        buttonText: Get started
      - title: Real-time Data Pipelines In One Command
        icon: plug-cloud
        text: Continuous Query Notifications for streaming data
        url: /sign-up
        buttonText: Get started
  callsToAction:
    deliverablesHoldingYouBack:
      title: Are You Aiming To Be Held Back By Your Deliverables?
      subtitle: Buy the finished Ferrari, not the kit car
      buttonText: Learn More >>>
      buttonLink: /in-detail
    notSelfService:
      title: |
        Can Every SaaS Provider Be Trusted With Your Data?
      subtitle: Doing it yourself is easy with Halfpipe
      buttonText: Learn More >>>
      buttonLink: /in-detail
    infrastructureDoneForYou:
      title: Data Integration And Infrastructure Done For You
      subtitle: Your data in Snowflake easily, using tried and tested patterns
      buttonText: How To Buy >>>
      buttonLink: /services
    kubernetes:
      title: Finally, Scalable Data Integration
      subtitle: Fits In AWS Lambda And It's Kubernetes Ready
      buttonText: Learn More >>>
      buttonLink: /in-detail
    seeFeatures:
      title: "More Features Of Halfpipe In Action"
      subtitle: Discover features in a series of videos
      buttonText: Features >>>
      buttonLink: /features
  features:
    # old unused features commented on the in-detail page:
    ripSchema:
      title: Migrate An Entire Oracle Schema, In One Command
      subtitle: Get straight to Snowflake via S3
      image: hp-oracle-full-schema-cp-snap.svg
      buttonText: Learn More >>>
      buttonLink: /sign-up
    syncBatch:
      title: Synchronise Your Tables, In One Command
      subtitle: Reconcile millions of rows in minutes
      image: hp-sync-batch-oracle-snowflake.svg
      buttonText: Learn More >>>
      buttonLink: /sign-up
    microservice:
      title: Data Integration as a Micro-Service
      subtitle: '"DIaaMs," is that a thing!?'
      image: hp-serve-cp-delta-snowflake.svg
      buttonText: Learn More >>>
      buttonLink: /sign-up
  material:
    old-unused:
      waysOfWorking:
        title: The Simple Way Of Working
        subtitle: Complex data integration patterns made simple
        features:
          - title: Runs The Same Locally As In Production
            icon: laptop-globe
            text: |
              A single binary to run both locally and in production. Do it the same everywhere to reduce the complexity of your
              tech stack and overhead management costs.
              <br>
              <br>
              Developers like to run the same stack locally as they do in production. It reduces the chance of error when
              deploying changes to live environments and helps with accurate and quick debugging when live issues pop up.
              <br>
              <br>
              Halfpipe runs the same locally as in production.
          - title: Scale Out
            icon: laptop-globe
            text: |
              If you were to build a house out of different sized blocks it wouldn't produce stable results, or be as easy to do as with
              good old-fashioned, uniform-sized bricks. It's imporant to keep things the same if you want to scale.  Halfpipe always operates
              at the table level. Use it multiple times to build your new warehouse in the cloud.
          - title: Not another SaaS product!
            icon: laptop-globe
            text: |
              Everytime you adopt a SaaS product you risk your data a little bit more.
              Use Halfpipe on your compute infrastructure and keep full control of your data assets.
          - title: Halfpipe Is The E and L in ELT
            icon: laptop-globe
            text: |
              Over the last few years, ELT has become common place because of the power of modern analytics databases.
              Halfpipe specialises in moving data easily, not transforming it. Do one thing and do it well!
              <br>
              <br>
              Best of breed tools like [dbt](https://blog.getdbt.com/what--exactly--is-dbt-/) are perfect for transforming your
              data and they complement Halfpipe.
      inProduction:
        title: In Production
        subtitle: Deploy at scale, it's Kubernetes ready!
        features:
          - title: Runs as a Micro-service
            icon: laptop-globe
            text: |
              Adopting micro-services patterns can be challenging.
              Halfpipe can run a micro-service ready for all your data engineers, developers and dev-ops teams alike to integrate.
          - title: Runs In AWS Lambda - It's Super Light Weight
            icon: laptop-globe
            text: |
              Cloud functions require a small deployment footprint compared to classic data integration applications.
              Cloud functions offer a great way to reduce infrastructure and mangement costs.
              <br>
              <br>
              Halfpipe fits in AWS Lambda. Run micro-ELT batches that are easy to reason about.
          - title: Builds Your Datalake
            icon: laptop-globe
            text: |
              It's a good idea to keep your data on a platform that makes it an asset for future use.
              Storing data in AWS S3 is one of the lowest cost and most reliable ways to do this.
              There's a huge ecosystem of tools and services that can leverage it.
              <br><br>
              Halfpipe lets you build out your datalake in S3 while giving you the ability to extract from Oracle
              and load into Snowflake in one hop!
          - title: Easy To Deploy
            icon: laptop-globe
            text: |
              Halfpipe is a super light-weight binary that's easy to deploy. Use your favourite scheduler to run it.
              Airflow and AWS CodeBuild are great examples. It's Kubernetes ready too. Or trigger it to run inside AWS
              Lambda to reduce your costs.
      cost:
        title: Reduce Cost By Simplifying
        subtitle: Use pre-canned ELT patterns to reduce your complexity
        features:
          - title: From Oracle To Snowflake, In One Command
            icon: laptop-globe # icon from /images/illustrations/icons/___.svg)
            text: |
              Writing ETL patterns to get from Oracle to Snowflake is a complex multi-step process that's time consuming.
              Save time by using Halfpipe and do it in one step.
          - title: \"No One Wants Data Integration Teams\"
            icon: laptop-globe
            text: |
              Ultimately, they just want access to their data in a place that unlocks the most value.
              <br>
              <br>
              Accellerate your DataOps teams so they don't become the new bottle-neck.
              Halfpipe contains the pre-built ELT patterns so you don't have re-invent the wheel.
          - title: |
              "Real-Time Data Is An Expensive Ideal To Aim For...<br>
              Near-Real Time Is Normally Good Enough"
            icon: laptop-globe
            text: |
              Building real-time data feeds means system complexity increases and it's relatively hard to get right.
              Especially compared to near-real time, frequent batch processing.
              <br>
              <br>
              More often than not, near-real time is the right balance of speed and cost.
              <br>
              <br>
              If you're migrating to a new world of micro-services, or just moving data to leverage
              the cloud, then you'll turn off the source data feeds once you've migrated.
              Avoid wasted time and cost developing pure real-time solutions that you will just turn off.
              <br>
              <br>
              For slowly changing data, Halfpipe gives you a real-time event feed in one command.
              For everything else, it contains the near-real time patterns that you need, also available in one command.
          - title: Log Mining Is Expensive. You Think You Need To Capture Deleted Records
            icon: laptop-globe
            text: |
              Log mining is costly to set up and manage, and you always have the problem that an initial load that differs from
              the ongoing update to your target tables. It's this two-pronged approach that costs in the long run -
              in development effort and slow rebuild times.
              <br>
              <br>
              You save resources by adopting a one-pronged approach that's consistent and transactionsal. It speeds
              up your development teams.
              <br>
              <br>
              So where you just have 10s of millions of rows per table, you can use Halfpipe to compare and sync data in one command.
              It's the same command for an initial load as it is for ongoing updates. Halfpipe takes care of sync'ing the deltas.
              <br>
              <br>
              For larger datasets it pays to capture deleted records in an audit trail so you can use a one-pronged
              again and avoid complex transaction log management scenarios.
              Halpipe contains the near-real time patterns that you need to make this happen quickly, without
              development effort.
      upAndRunningFast:
        title: Features, Fast
        subtitle: Complex ELT patterns wrapped up in single commands
        features:
          - title: Migrate Data Structures In One Command
            icon: laptop-globe
            text: |
              Setting up new data structures is a boring pre-requisite compared to seeing your data assets in a
              new shiny cloud-native database, ready to use.
              <br>
              <br>
              Halfpipe makes this fun again by reading source DDL, making it compatible with the target system and
              letting you copy & execute in one command!
          - title: Synchronise Tables Into Snowflake In One Command
            icon: laptop-globe
            text: |
              When data changes in a source system and you want your target databases to receive updates, it can be tricky
              to take care of physically deleted records.
              You either need to invest in log mining or set up an audit trail in your source system.
              <br>
              <br>
              Both of these options are engineering overkill for small tables of 10 million rows or less.
              <br>
              <br>
              Halfpipe lets you compare and synchronise source and target tables in one command, in-stream,
              without buffering or memory usage going off the chart!
          - title: Update Table Data In One Command
            icon: laptop-globe
            text: |
              It's good to use the "database" for what it does best, like generating an audit trail of record changes when
              you need one.
              <br>
              <br>
              Using this approach helps you simplify your engineering efforts by keeping "initial loads"
              the same as the process for applying chnages to a target database. It helps your teams to scale better.
              <br>
              <br>
              Halfpipe lets you turn the audit trails into incremental data feeds to update your target in one command.
          - title: Snapshot Tables Into Snowflake In One Command
            icon: laptop-globe
            text: |
              When an analyst needs to get a new table into a database as a one-off, it's painful if your ELT tools slow
              you down. It should be simple to copy a table to get up and running fast, right?
              <br>
              <br>
              Halfpipe allows you to copy snapshots of tables between RDBMS in one command, and run that in a loop to keep
              the target up-to-date too.
          - title: Real-time Event Streams In One Command
            icon: laptop-globe
            text: |
              Real-time data feeds are complex to engineer, but they're easy in Halfpipe when the use-case is right.
              <br>
              <br>
              Halfpipe can spin up a real-time stream of data changes from Oracle to Snowflake in one command.

    simpleTech:
      title: "It's Simple Tech"
      subtitle:
      backgroundColour: section-feature-grey
      features:
        - title:
          icon: "/images/noun_canned_916245-400x400.png"
          altText: "Image of Canned by Nook Fulloption from the Noun Project"
          text: |
            Use pre-canned data integration & validation patterns wrapped into just one tiny command line tool.
            Buy the finished Ferrari, not the kit car
        - title:
          icon: "/images/noun_Blockchain_3097580.png"
          altText: "Image of Blockchain by Adrien Coquet from the Noun Project"
          text: |
            Compatible with on-prem, cloud-compute and serverless architectures
        - title:
          icon: "/images/noun_everywhere_2120639.png"
          altText: "Image of Everywhere by priyanka from the Noun Project"
          text: Runs the same locally as in production. A single binary to use everywhere so you’ll simplify your tech stack and reduce costs
        - title:
          icon: "/images/noun_grid_397475.png"
          altText: "Image of Grid by icon 54 from the Noun Project"
          text: Operates at the table level so you can use it multiple times to scale out
    easyToUse:
      title: "Easy To Use"
      subtitle:
      backgroundColour: section-white
      features:
        - title:
          icon: "/images/noun_easy_2036017.png"
          altText: "Image of Easy by priyanka from the Noun Project"
          text: Migrate a schema, take snapshots or deltas, create real-time pipelines or perform data validation all in just one command
        - title:
          icon: "/images/noun_Heart_1703695.png"
          altText: "Image of Heart by Leona Grande from the Noun Project"
          text: Anyone can work with it, not just your DataOps teams
        - title:
          icon: "/images/noun_lightweight_2858695.png"
          altText: "Image of Lightweight by supalerk laipawat from the Noun Project"
          text: It’s a super lightweight binary that’s easy to deploy. Run it in AWS Lambda or choose your favourite scheduler. It’s Kubernetes ready too
        - title:
          icon: "/images/noun_Privacy_3427755.png"
          altText: "Image of Privacy by SBTS from the Noun Project"
          text: Easier compliance with GDPR, CCPA and HIPPA etc by running in your own cloud
    savesYouMoney:
      title: "It Will Save You Money"
      subtitle:
      backgroundColour: section-feature-grey
      features:
        - title:
          icon: "/images/noun_sourcing_2903383.png"
          altText: "Image of Sourcing by Eucalyp from the Noun Project"
          text: Get real-time and near real-time data feeds with just one simple command. Zero development required
        - title:
          icon: "/images/noun_Countdown_50361.png"
          altText: "Image of Countdown from the Noun Project #50361"
          text: Save your developers time with a ready-made tool that’s quick and easy to use
        - title:
          icon: "/images/noun_Scissors_3005373.png"
          altText: "Image of Scissors by Eucalyp from the Noun Project"
          text: Try out Snowflake and keep full control of your data. Move it into the cloud yourself, without having to use a third party
        - title:
          icon: "/images/noun_service_429918.png"
          altText: "Image of Service by Gregor Cresnar from the Noun Project"
          text: Runs as a microservice to instantly fit in with your other apps and systems
        - title:
          icon: "/images/noun_lambda_1488175.png"
          altText: "Image of lambda by LAFS from the Noun Project"
          text: Ready to run in AWS Lambda. Move one billion records a day for as little as [~$16 a month](../blog/06-using-serverless-to-reduce-cost-aws-lambda)
        - title:
          icon: "/images/noun_gate_2869442.png"
          altText: "Image of gate by priyanka from the Noun Project"
          text: Allows self-hosting and reduces egress costs. Avoid the row-based pricing of other SaaS products