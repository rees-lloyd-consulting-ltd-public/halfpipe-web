---
title: "Halfpipe: Video"
layout: funnel-vsl
heading: "What Is Halfpipe?"
subtitle: "Learn why I created it, the problem it solves and how it helps engineers build & run reliable data platforms"
buttonText: "Apply Now >>>"
footerText: |
    To learn more about how Halfpipe can work for you, click Apply Now below to schedule a call. Alternatively,
    head over to my [contact page](/contact) to drop me a line.
redirectToUsingCalendly20220416: schedule.md
redirectTo: contact.md
---
