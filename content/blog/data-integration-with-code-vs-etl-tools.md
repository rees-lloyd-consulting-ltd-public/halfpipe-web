---
draft: true
author: Richard Lloyd
sidebar: true
title: "Halfpipe: Whether To Code Your Data Integration Or Use An ETL Tool?"
blogTitle: "From Writing Oracle PL/SQL To Move Data Around, To Building A Scalable Data Integration Platform Using Pentaho (PDI)"
subtitle: "To Code or Re-use?"
image: "randy-fath-ymf4_9Y9S_A-unsplash.jpg"
date:
breadcrumbs:
    home: 
        name: Back To Blog Home
        link: /blog
---

### Let's Start With The Code...

Twenty years ago I started writing PL/SQL to synchronise data across Oracle databases and I loved it. It put me close to the metal and I learnt how to move records fast and as efficiently as possible.

Eventually I landed a role working for a bank in central London where I spent three months writing PL/SQL to populate a new data warehouse with IT event data distributed across the globe.  It did the job.  The business had the data they needed to help improve the quality of their first line service desk and the apps they supported.  But it could have been done a whole lot quicker!

### 10x Speed-Up With Pentaho Data Integration

My role evolved and I started using Pentaho Data Integration (it's a Swiss Army Knife for data) as the second member of a team building a platform for 300 apps to share data.

It was there that I realised I could have used Pentaho to build my data warehouse from earlier, including all of the PL/SQL to keep it up to date, in just a week.

It blew my mind!

### Scalable Data Integration Platforms

I went on to grow the data integration platform to synchronise 100s of billions of records weekly from over 30 sources.  Our pipelines were integrated with bank wide monitoring and alerting systems and it was all fully supportable by ONLY TWO members of staff.

We attributed our success to the fact that we built systems that scaled with as few moving parts as possible.  We shunned storing state where it wasn't required and made sure our data pipelines were always idempotent.

We wanted them to just pick up where they left off, if any of the moving parts failed at any point for any reason.  We didn't want to maintain anything and this meant we were free to grow the platform and innovate around analytics for the business.

### There's A Problem With ETL Tools...

The thing with off-the-shelf ETL tools is that they promise to be the Ferrari of cars (or your personal favourite, of course).  It's only when you get hold of one that you realise you've just bought a kit!  

It seems you're getting re-use by adopting an ETL tool, but you still have to build the solution!  And you're going to need a team of experienced mechanics to do it.  

Building the solution is only just the beginning; you will need to test and maintain it too.

I believe everyone should have access to the data integration techniques that I've learnt and found to be reliable in my career so far.

If you want to recreate this, there are a lot of lessons to be learned along the way.  And the chances of making mistakes are high, especially in terms of wasted time and resources.

Not everyone can specialise in everything so I've put all my experience together and created a simple system that's straight forward for any engineer to pick up and reuse.  

Wouldn't it be great if you could get legendary-mode data-ops skill wrapped into a single tool?

### The Solution For Everyone

So I spent the past week putting together a training video showing how you can get direct access to the pre-built Ferrari.  It's currently purpose-built to speed up your journey to Snowflake.

If you're interested to level-up your data integration capabilities, either personally or to help your business improve access to its data, then this is for YOU!

Click the link below to view for the free training video.

Don't worry, there's no hidden cost and I won't be selling your details to anyone - I'm a professional and want it to stay that way 😄

[>> How To Get To Oracle To Snowflake Without Building Data Integration]({{< ref "/enterprise/get-to-snowflake-without-building-data-integration" >}})

<br>

Thanks

-- Richard