---
title: "Halfpipe: Start a Microservice"
blogTitle: Start a Microservice
subtitle: Easily control and monitor your pipelines in production
image: noun_service_429918.png
altText: Image of Service by Gregor Cresnar from the Noun Project
breadcrumbs:
    home: 
        name: Back To Features Home
        link: /features
---
 
In this demo of Halfpipe as-a-microservice, we launch an incremental pipeline to keep a 
Snowflake table up-to-date.

The video quality is low, but there's some audio commentary worth listening to. 

{{< youtube-video "https://www.youtube.com/embed/z8AFGjsVd4c" >}}

For better quality video, here's an alternative link to the [original animation](https://github.com/relloyd/halfpipe/tree/master/demo-svg/service) 
on GitHub (no audio).
