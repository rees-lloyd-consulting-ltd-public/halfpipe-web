---
title: "Halfpipe: Synchronise Tables & Data"
blogTitle: Synchronise Tables & Data
subtitle: Make the contents of a target table look like the source, in one command
image: noun_change_435217.png
altText: Image of Change by Alebaer from the Noun Project
breadcrumbs:
    home: 
        name: Back To Features Home
        link: /features
---

#### Sync Table Data From Oracle To Snowflake

{{< youtube-video "https://www.youtube.com/embed/4-Y1lD_GxS0" >}}

#### Sync Table Data Between Oracle Databases

{{< youtube-video "https://www.youtube.com/embed/RIHcPuG7xcY" >}}
