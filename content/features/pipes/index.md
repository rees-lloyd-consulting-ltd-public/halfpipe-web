---
title: "Halfpipe: Config Files For Halfpipe Actions"
blogTitle: Create A Config File For A Halfpipe Action
subtitle: Post them to the Halfpipe microservice or use them to customise SQL
image: noun_File_1229925.png
altText: Image of File by DinosoftLab from the Noun Project
breadcrumbs:
    home: 
        name: Back To Features Home
        link: /features
---

#### How To Generate A Config File For An Action And Run It

{{< youtube-video "https://www.youtube.com/embed/2BZh585y08U" >}}
