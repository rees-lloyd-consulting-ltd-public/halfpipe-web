---
title: "Halfpipe: Data Validation"
blogTitle: Data Validation
subtitle: Easily compare records across two tables and report on the differences, in one command 
image: noun_comparison_2215834.png
altText: Image of Compare by Manthana Chaiwong from the Noun Project
breadcrumbs:
    home: 
        name: Back To Features Home
        link: /features
---

#### How To Validate Tables Contain The Same Data

Use the `hp diff` command to compare records across source and target tables.

Differences are output in JSON format separated by new lines,
where the source is considered the new dataset and the target is the old
(reference) data.

- Supply the primary key fields to sort and join the two datasets for comparison
- Optionally choose the number of differences allowed before exiting




{{< youtube-video "https://www.youtube.com/embed/eRpRA-EpI6Q" >}}
