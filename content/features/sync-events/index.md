---
title: "Halfpipe: Stream Oracle Table Changes in Real-time"
blogTitle: Stream Oracle Table Changes in Real-time
subtitle: Event-driven data from Oracle to Snowflake, in one command
image: noun_play_3002899_505x.png
altText: Image of Play by Honey Wadhwani from the Noun Project
breadcrumbs:
    home: 
        name: Back To Features Home
        link: /features
---

#### Sync Events In Real-time, From An Oracle Table To Oracle Or Snowflake

This action keeps a target updated in real-time with new, changed and deleted records found in a source.

It works in real-time where the source transaction size is 100 records or less. 

And when more records change per source transaction, it automatically kicks off a full re-sync of data using the [sync batch](../sync-batch) action. 

This is great for slowly changing data!

{{< youtube-video "https://www.youtube.com/embed/-ID6mnF78JA" >}}
